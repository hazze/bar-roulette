import 'dotenv/config';
import * as express from "express";
import * as bodyParser from "body-parser";
import { Routes } from "./routes/routes";
import * as cors from "cors";

const options: cors.CorsOptions = {
	allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token", "*"],
	credentials: true,
	methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
	origin: '*',
	preflightContinue: false
};


class App {
	public app: express.Application;
	public routePrv: Routes = new Routes();

    constructor() {
        this.app = express();
		this.config();
		this.routePrv.routes(this.app);
		this.app.use(cors(options));
		// this.app.options('*', cors(options));
    }

	


    private config(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
}

export default new App().app;