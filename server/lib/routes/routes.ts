import { Request, Response } from "express";
import { BarsController } from "../controllers/BarsController";



export class Routes {   
	barsController = new BarsController();

    routes(app): void {         
		app.use((req, res, next) => {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			next();
		  });

		app.route('/api/bars/fetch/')
			.post(this.barsController.getBars)
    }
}