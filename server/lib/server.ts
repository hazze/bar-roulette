import app from "./app";
const PORT = 4444;

app.listen(PORT, () => {
	console.log('Express server listening on port ' + PORT);
})