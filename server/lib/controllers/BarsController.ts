import { Request, Response } from "express";

var XMLHttpRequest = require('xhr2');
var getJSON = (url, callback) => {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

export class BarsController {
	

	getBars(req: Request, res: Response) {
		getJSON(req.body.url, (err, data) => {
			if (err !== null) {
				alert('Something went wrong: ' + err);
			} else {
				res.json(data);
				res.status(200);
				res.send();
			}
		});
	}
}