export interface User {
	id: number;
	firstName: string;
	lastName: string;
	ssn: string|null; //Only for applicants
	email: string|null; //Only for applicants
	username: string|null; // Only for recruiters
	password: string;
	role_id: number;
}