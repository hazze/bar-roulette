import Vue from 'vue'
import Component from 'vue-class-component'
import { mapGetters } from 'vuex';

@Component({
  computed: mapGetters({
    estType: "getType"
})})
export default class HomeComponent extends Vue {

    estType!: string;

    mounted() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(this.displayLocationInfo);
      }
    }

    setButtonText(): string {
      switch (this.estType) {
        case "bar":
          return "bar";
        case "restaurant":
          return "restaurant";
        case "night_club":
          return "nightclub";
        default:
          return "";
      }
    }

    displayLocationInfo(position: any) {
      const location = {lat: position.coords.latitude, lng: position.coords.longitude}
      this.$store.dispatch('saveUserLocation', location);
    }
    goToBarPage() {
        this.$emit('spinned')
    }
    openFilterDialog() {
        this.$emit('openFilterDialog')
    }

}
