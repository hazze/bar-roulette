import Vue from 'vue';
import Component from 'vue-class-component';
import buildUrl from 'build-url';
import { mapGetters } from 'vuex';
import { Filter } from '@/models/Filter';
import { Watch } from 'vue-property-decorator';
import dataService from '@/data-service';
import { Bar } from '../../models/Bar';



@Component({
    computed: mapGetters({
      googleURL: "getURL",
      filter: "getFilters",
      filterChange: "getFilterChange",
      storeBarList: "getBar",
      estType: "getType"
})})
export default class BarPageComponent extends Vue {
    googleURL!: string;
    filter!: Filter;
    filterChange!: boolean;
    storeBarList!: [];
    barData: any = null;
    barList: Bar[] = [];
    randomBar: any = null;
    photoRef: string = '';
    imgurl: string = '';
    estType!: string;

    // Set default meta data
    lat: number = 59.331254;
    lng: number = 18.062142;
    name: string = '';
    priceRange: number = 0;
    address: string = '';
    rating: number = 0;
    radius: number = 1000;
	markers: any = [];
	nothing: boolean = false;

    setButtonText(): string {
      switch (this.estType) {
        case "bar":
          return "bar";
        case "restaurant":
          return "restaurant";
        case "night_club":
          return "nightclub";
        default:
          return "";
      }
    }

    async mounted() {
      await this.randomizeBar();
    }
    setMetaInfo() {
		if (this.randomBar) {
			this.nothing = false;
			this.lat = this.randomBar.geometry.location.lat
			this.lng = this.randomBar.geometry.location.lng
			this.name = this.randomBar.name;
			this.priceRange = this.randomBar.price_level;
			this.address = this.randomBar.vicinity;
			this.rating = this.roundRating(this.randomBar.rating, 0.5);
			this.photoRef = this.randomBar.photos ? this.randomBar.photos[0].photo_reference : "";
		} else {
			this.nothing = true;
		}
    }
    roundRating(value: number, step: number) {
      // tslint:disable-next-line:no-unused-expression
      step || (step = 1.0);
      const inv = 1.0 / step;
      return Math.round(value * inv) / inv
    }
    async getGoogleJSON(url: string) {
		let data = {};
		await dataService.getBar(url)
			.then(x => {
				data = x.data
			});
		return data;
    }

	async getSecondaryPages() {
		const next = this.googleURL.concat(`&pagetoken=${this.barData.next_page_token}`)
		this.barData = await this.getGoogleJSON(next);
		this.barList = this.barList.concat(this.barData.results);
		this.$store.dispatch('saveBarList', this.barList);
		if (this.barData.hasOwnProperty('next_page_token')) {
			setTimeout(async () => await this.getSecondaryPages(), 2000);
		}
	}

    async randomizeBar() {
      this.saveAPIURL();
      if (this.filterChange) {
		this.barData = await this.getGoogleJSON(this.googleURL);
		this.barList = this.barData.results;
		if (this.barData.hasOwnProperty('next_page_token')) {
			setTimeout(async () => await this.getSecondaryPages(), 2000);
		}
		this.$store.dispatch('resetFilterChange')
		this.$store.dispatch('saveBarList', this.barList)
	}
      this.randomBar = this.storeBarList[Math.floor(Math.random() * this.storeBarList.length)];
      this.setMetaInfo();
      this.getPhotoFromReference();
    }

    async getPhotoFromReference() {
      const url = buildUrl('https://maps.googleapis.com', {
        path: '/maps/api/place/photo',
        queryParams: {
          maxwidth: "400",
          photoreference: this.photoRef,
          key: process.env.VUE_APP_GOOGLE_API_KEY
        }
      });
      this.imgurl = url !== undefined ? url : "https://i.imgur.com/L4X4Gc5.jpg";
    }

    openFilterDialog() {
        this.$emit('openFilterDialog');
	}

    saveAPIURL() {
      	const url = buildUrl('https://maps.googleapis.com', {
			path: 'maps/api/place/nearbysearch/json',
			queryParams: {
				location: `${this.filter.location.lat},${this.filter.location.lng}`,
				radius: this.filter.radius.toString(),
				type: this.filter.type,
				minPrice: this.filter.minPrice.toString(),
				maxPrice: this.filter.maxPrice.toString(),
				rating: this.filter.rating.toString(),
				opennow: 'true',
				key: process.env.VUE_APP_GOOGLE_API_KEY
			}
		});
		     this.$store.dispatch('saveURL', url);
    }

    getRoute() {
      window.location.href = `https://www.google.com/maps/dir/My+Location/${this.lat},${this.lng}`
    }
}
