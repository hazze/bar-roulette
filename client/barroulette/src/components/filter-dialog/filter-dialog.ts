import Vue from 'vue';
import Component from "vue-class-component";
import { Prop } from 'vue-property-decorator';
import { Filter } from '@/models/Filter';
import { Location } from '@/models/Location';
import { LocationEnum } from '@/models/Constants';

interface FilterLocationViewModel {
  name: string;
  value: LocationEnum;
}
interface FilterTypeViewModel {
  name: string;
  value: string;
}

@Component({})
export default class FilterDialogComponent extends Vue {

	@Prop()
  filterDialogVisible!: boolean;

  filter = {} as Filter;
  sodermalm: Location = {lat: 59.312227, lng: 18.063408};
  ostermalm: Location = {lat: 59.337437, lng: 18.090661};
  norrmalm: Location = {lat: 59.333488, lng: 18.056468};
  myLocation: Location = {lat: 0, lng: 0};
  regions: FilterLocationViewModel[] = [
    {name: "Current location", value: LocationEnum.MyLocation},
    {name: "Södermalm", value: LocationEnum.Södermalm},
    {name: "Östermalm", value: LocationEnum.Östermalm},
    {name: "Norrmalm", value: LocationEnum.Norrmalm}
  ];
  region: LocationEnum = 0;

  types: FilterTypeViewModel[] = [
    {name: 'Bar', value: 'bar'},
    {name: 'Restaurant', value: 'restaurant'},
    {name: 'Nightclub', value: 'night_club'}
  ];
  radius: number = 1000;
  type: string = this.types[0].name
  price: number[] = [1, 4]
  rating: number = 1;

	  hideDialog() {
		  this.$emit("hideDialog");
    }

    async mounted() {
      this.getUserPosition();
    }

    getUserPosition() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(this.displayLocationInfo);
      }
    }

    displayLocationInfo(position: any) {
      this.myLocation.lng = position.coords.longitude;
      this.myLocation.lat = position.coords.latitude;
    }

    saveFilters() {
      this.hideDialog();
      switch (this.type) {
        case this.types[0].name:
          this.filter.type = this.types[0].value
          break;
        case this.types[1].name:
          this.filter.type = this.types[1].value
          break;
        case this.types[2].name:
          this.filter.type = this.types[2].value
          break;
        default:
          this.filter.type = 'bar'
          break;
      }
      this.filter.minPrice = this.price[0];
      this.filter.maxPrice = this.price[1];
      switch (this.region) {
        case LocationEnum.Södermalm:
          this.filter.location = this.sodermalm;
          break;
        case LocationEnum.Östermalm:
          this.filter.location = this.ostermalm;
          break;
        case LocationEnum.Norrmalm:
          this.filter.location = this.norrmalm;
          break;
        case LocationEnum.MyLocation:
          this.getUserPosition();
          this.filter.location = {lat: this.myLocation.lat, lng: this.myLocation.lng}
          break;
        default:
          console.log("DEFAULT");
          break;
      }
      this.filter.radius = this.filter.radius || 2000
      this.filter.rating = this.filter.rating || 2
      const f: Filter = Object.assign({}, this.filter as Filter);
      // console.log("filter: ", f)
      this.$store.dispatch('saveFilter', f)
    }
}
