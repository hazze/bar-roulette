import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator'


@Component({})
export default class NavigationComponent extends Vue {
	@Prop()
	drawer!: any;

	closing() {
		this.$emit("closing");
	}

	openLoginDialog() {
		this.$emit("openLoginDialog");
	}

	openRegisterDialog() {
		this.$emit("openRegisterDialog");
	}

	goToHome() {
		this.$emit("closing");
		this.$emit("changeToHome");
	}
	goToAbout() {
		this.$emit("closing");
		this.$emit("changeToAbout");
	}
}
