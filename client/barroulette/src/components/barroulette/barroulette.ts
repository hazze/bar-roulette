import Vue from 'vue';
import Component from 'vue-class-component';
import NavigationComponent from '../navigation/navigation';
import LoginDialogComponent from '../login-dialog/login-dialog';
import HomeComponent from '../home/home';
import { NavigationViewsEnum } from '@/models/Constants';
import RegisterDialogComponent from '../register-dialog/register-dialog';
import BarPageComponent from '../barpage/barpage';
import FilterDialogComponent from '../filter-dialog/filter-dialog'
import AboutComponent from '../about/about';

@Component({
	components: {
		NavigationComponent,
		LoginDialogComponent,
		RegisterDialogComponent,
		HomeComponent,
		BarPageComponent,
		AboutComponent,
		FilterDialogComponent,
	}
})
export default class BarRouletteComponent extends Vue {
	navigationViews = NavigationViewsEnum;
	drawer: any = null;
	loginDialogVisible: boolean = false;
	registerDialogVisible: boolean = false;
	filterDialogVisible: boolean = false;
	activeView: NavigationViewsEnum = NavigationViewsEnum.Home;

	setActiveView(view: NavigationViewsEnum) {
		this.activeView = view;
	}

	hideDialog() {
		this.loginDialogVisible = false;
	}

	openLoginCloseNav() {
		this.drawer = !this.drawer;
		this.loginDialogVisible = true;
	}

	openRegisterCloseNav() {
		this.drawer = !this.drawer;
		this.registerDialogVisible = true;
	}

	openFilterDialog() {
		this.filterDialogVisible = true;
	}
}
