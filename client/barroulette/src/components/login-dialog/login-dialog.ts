import Vue from 'vue';
import Component from "vue-class-component";
import { Prop } from 'vue-property-decorator';

@Component({})
export default class LoginDialogComponent extends Vue {
	@Prop()
	loginDialogVisible!: boolean;

	hideDialog() {

		this.$emit("hideDialog");
	}
}
