import Vue from 'vue';
import Component from "vue-class-component";
import { Prop } from 'vue-property-decorator';

@Component({})
export default class RegisterDialogComponent extends Vue {
	@Prop()
	registerDialogVisible!: boolean;

	hideDialog() {
		this.$emit("hideDialog");
	}
}
