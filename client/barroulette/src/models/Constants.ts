enum NavigationViewsEnum {
	Home = 1,
	BarPage = 2,
	About = 3,
	Contact = 4,
}

enum LocationEnum {
	MyLocation = 0,
	Södermalm = 1,
	Östermalm = 2,
	Norrmalm = 3
}

enum FilterTypeEnum {
	bar = 0,
	restaurant = 1,
	night_club = 2
}


export { NavigationViewsEnum, LocationEnum, FilterTypeEnum}
