import { Location } from '@/models/Location'

export interface Filter {

    minPrice: number;
    maxPrice: number;
    type: string;
    location: Location;
    rating: number;
    radius: number;
}
