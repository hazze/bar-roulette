import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import store from './store';
import axios from 'axios';
import VueAxios from 'vue-axios';
import * as VueGoogleMaps from 'vue2-google-maps';
// import './registerServiceWorker'

Vue.config.productionTip = false;
Vue.use(VueAxios, axios)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDgr1hXJhi8_eZAONXezxfJc-z85YkPhn4',
    libraries: 'places', // This is required if you use the Autocomplete plugin
}});

new Vue({
//   router,
  store,
  render: (h) => h(App),
}).$mount('#app');


