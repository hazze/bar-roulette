import axios from 'axios';
import { AxiosInstance, AxiosPromise } from 'axios';


export default new class DataService {
	initialize(): AxiosInstance {
		let ax: AxiosInstance;
		ax = axios.create({
			baseURL: process.env.VUE_APP_SERVER_API,
			params: {},
			headers: {
				'Content-Type': "application/json",
				'Access-Control-Allow-Origin': '*',
				"Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
				'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
			}
		});

		// ax.interceptors.response.use(undefined, (error: any) => {
		// 	const code = (error && error.response && error.response.status) ? error.response.status : "";
		// 	const message = (error &&  error.message) ? error.message : "";
		// 	const data = (error && error.response && error.response.data) ? error.response.data : "";
		// 	console.log("HTTP.interceptor", error);
		// 	return Promise.reject(error);
		// });

		// ax.interceptors.request.use(
		// 	(config) => {
		// 	  const user = localStorage.getItem('token') ? localStorage.getItem('token') : null;
		// 	  const token = JSON.parse(<string> user);
		// 	  if (token) {
		// 		config.headers['Authorization'] = token.token;
		// 	  }
		// 	  return config;
		// 	},
		// 	(error) => {
		// 	  return Promise.reject(error);
		// 	}
		// );
		return ax;
	}

	getBar(url: string): AxiosPromise<any> {
		const obj = {
			url
		}
		const http = this.initialize();
		return http.post('bars/fetch/', JSON.stringify(obj));
	}
}
