import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    barSelection: {
      location: {lat: 59.331254, lng: 18.062142},
      radius: 1000,
      type: 'bar',
      minPrice: 1,
      maxPrice: 4,
      rating: 1,
    },
    filter: {
      changed: true
    },
    placesURL: {
      url: ''
    },
    bar: {
      list: []
    }
  },
  mutations: {
    saveFilter(state, filters) {
      state.barSelection = filters
      state.filter.changed = true
    },
    saveURL(state, url) {
      state.placesURL.url = url
    },
    saveUserLocation(state, location) {
      state.barSelection.location = location
    },
    resetFilterChange(state) {
      state.filter.changed = false
    },
    saveBarList(state, barlist) {
      state.bar.list = barlist
    },
  },

  actions: {
    saveFilter(context, filters) {
      context.commit('saveFilter', filters)
    },
    saveURL(context, url) {
      context.commit('saveURL', url)
    },
    saveUserLocation(context, location) {
      context.commit('saveUserLocation', location)
    },
    resetFilterChange(context) {
      context.commit('resetFilterChange')
    },
    saveBarList(context, barlist) {
      context.commit('saveBarList', barlist);
    }
  },
  getters: {
    getFilters: (state) => state.barSelection,
    getFilterChange: (state) => state.filter.changed,
    getURL: (state) => state.placesURL.url,
    getBar: (state) => state.bar.list,
    getType: (state) => state.barSelection.type
  },
});
