module.exports = {
	// publicPath: process.env.NODE_ENV === 'production' ? '/production-path/' : '/',
    // ...other vue-cli plugin options...
    pwa: {
        // configure the workbox plugin
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            // swSrc is required in InjectManifest mode.
            swSrc: 'public/service-worker.js',
            // ...other Workbox options...
        }
	},
	devServer: {
		open: process.platform === 'darwin',
		host: '0.0.0.0',
		port: 8080, // CHANGE YOUR PORT HERE!
		https: false,
		hotOnly: false,
		disableHostCheck: true
	  },
}